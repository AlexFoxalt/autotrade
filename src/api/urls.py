from django.urls import path, include
from rest_framework import routers

from api.views import (
    PublicationRetrieveView,
    PublicationListView,
    PublicationCreateUpdateDeleteView,
    ClientListView,
    ClientCreateView,
    ClientRetrieveView,
    ClientUpdateDeleteView,
)

router = routers.DefaultRouter()

# fmt: off
urlpatterns = [
    path('', include(router.urls)),
    path('auth/', include("djoser.urls")),
    path('auth-jwt/', include("djoser.urls.jwt")),

    path("publication/list/", PublicationListView.as_view(), name="publication_list"),
    path("publication/create/", PublicationCreateUpdateDeleteView.as_view(), name="publication_create"),
    path("publication/<slug:slug>/retrieve/", PublicationRetrieveView.as_view(), name="publication_retrieve"),
    path("publication/<slug:slug>/update/", PublicationCreateUpdateDeleteView.as_view(), name="publication_update"),
    path("publication/<slug:slug>/delete/", PublicationCreateUpdateDeleteView.as_view(), name="publication_delete"),

    path("client/list/", ClientListView.as_view(), name="client_list"),
    path("client/create/", ClientCreateView.as_view(), name="client_create"),
    path("client/<uuid:uuid>/retrieve/", ClientRetrieveView.as_view(), name="client_retrieve"),
    path("client/<uuid:uuid>/update/", ClientUpdateDeleteView.as_view(), name="client_update"),
    path("client/<uuid:uuid>/delete/", ClientUpdateDeleteView.as_view(), name="client_delete")
]
# fmt: on
