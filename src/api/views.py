from rest_framework import status
from rest_framework.generics import (
    RetrieveAPIView,
    UpdateAPIView,
    DestroyAPIView,
    CreateAPIView,
    ListAPIView,
)
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework_simplejwt.authentication import JWTAuthentication

from api.serializers import ClientListSerializer
from services.decorators import admin_only
from services.mixins import PublicationAPIMixin, ClientAPIMixin


class PublicationListView(PublicationAPIMixin, ListAPIView):
    permission_classes = (AllowAny,)


class PublicationCreateUpdateDeleteView(
    PublicationAPIMixin, CreateAPIView, UpdateAPIView, DestroyAPIView
):
    authentication_classes = (JWTAuthentication,)


class PublicationRetrieveView(PublicationAPIMixin, RetrieveAPIView):
    permission_classes = (AllowAny,)


class ClientListView(ClientAPIMixin, ListAPIView):
    serializer_class = ClientListSerializer

    @admin_only
    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)


class ClientCreateView(ClientAPIMixin, CreateAPIView):
    def perform_create(self, serializer):
        client = serializer.save()
        client.set_password(serializer.data.get("password"))
        client.save()
        return serializer

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(
            serializer.data, status=status.HTTP_201_CREATED, headers=headers
        )

    @admin_only
    def post(self, request, *args, **kwargs):
        return self.create(request, *args, **kwargs)


class ClientRetrieveView(ClientAPIMixin, RetrieveAPIView):
    @admin_only
    def get(self, request, *args, **kwargs):
        return self.retrieve(request, *args, **kwargs)


class ClientUpdateDeleteView(ClientAPIMixin, UpdateAPIView, DestroyAPIView):
    @admin_only
    def put(self, request, *args, **kwargs):
        return self.update(request, *args, **kwargs)

    @admin_only
    def patch(self, request, *args, **kwargs):
        return self.partial_update(request, *args, **kwargs)

    @admin_only
    def delete(self, request, *args, **kwargs):
        return self.destroy(request, *args, **kwargs)
