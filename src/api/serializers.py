from rest_framework.serializers import ModelSerializer

from accounts.models import Client
from base.models import Publication


class PublicationSerializer(ModelSerializer):
    class Meta:
        model = Publication
        fields = "__all__"


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = "__all__"


class ClientListSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ("uuid", "email", "first_name", "last_name")
