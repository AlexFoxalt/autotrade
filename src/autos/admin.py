from django.contrib import admin  # noqa:

from autos.models import Announcement, Vehicle, VehiclePhoto
from autos.models_choices import City, Kind, Brand, Color, Condition

admin.site.register([Announcement, Vehicle, VehiclePhoto])
admin.site.register([City, Kind, Brand, Color, Condition])
