import json
import os
from json import JSONDecodeError
from time import sleep

from django.core.management.base import BaseCommand
from django.db import IntegrityError
from django.db.models.base import ModelBase

from autos.models_choices import City, Kind, Brand, Color, Condition
from services.loggers import colorize


def create_instances(objects: list, model: ModelBase):
    model_name = model._meta.verbose_name_plural.title()
    data = []
    for item in objects:
        data.append(model(name=item["item"]))
    model.objects.bulk_create(data)

    print(colorize("Yellow", f"Successfully created: {model_name}..."))
    sleep(0.1)


def create_all_instances(**kwargs):
    create_instances(kwargs.get("cities"), City)
    create_instances(kwargs.get("kinds"), Kind)
    create_instances(kwargs.get("brands"), Brand)
    create_instances(kwargs.get("colors"), Color)
    create_instances(kwargs.get("conditions"), Condition)


class Command(BaseCommand):
    help = "Fills the database with all the necessary information of choices"

    def handle(self, *args, **options):
        path_to_file = "autos/management/commands/db_data.json"

        if not os.path.isfile(path_to_file):
            return colorize("Red", "Error! db_data.json was not found!")

        with open(path_to_file, mode="r", encoding="utf-8") as f:
            try:
                data = json.load(f)
            except JSONDecodeError:
                return colorize(
                    "Red", "Error! Seems to be db_data.json is empty or modified!"
                )

        try:
            create_all_instances(
                cities=data.get("City"),
                kinds=data.get("Kind"),
                brands=data.get("Brand"),
                colors=data.get("Color"),
                conditions=data.get("Condition"),
            )
        except IntegrityError:
            return colorize("Red", "DB is already filled!")
        except TypeError:
            return colorize("Red", "Error! Some part of data was not found!")

        return colorize("Green", "Success!")
