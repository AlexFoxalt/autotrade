from django.db import models
from django.utils.translation import gettext_lazy as _


class Base(models.Model):
    name = None

    objects = models.Manager()

    class Meta:
        abstract = True

    def __str__(self):
        return self.name


class City(Base):
    name = models.CharField(_("Город"), max_length=100, unique=True, blank=False)

    class Meta:
        verbose_name = _("Город")
        verbose_name_plural = _("Города")


class Kind(Base):
    name = models.CharField(_("Тип"), max_length=100, unique=True, blank=False)

    class Meta:
        verbose_name = _("Тип")
        verbose_name_plural = _("Типы")


class Brand(Base):
    name = models.CharField(_("Бренд"), max_length=100, unique=True, blank=False)

    class Meta:
        verbose_name = _("Бренд")
        verbose_name_plural = _("Бренды")


class Color(Base):
    name = models.CharField(_("Цвет"), max_length=100, unique=True, blank=False)

    class Meta:
        verbose_name = _("Цвет")
        verbose_name_plural = _("Цвета")


class Condition(Base):
    name = models.CharField(_("Состояние"), max_length=100, unique=True, blank=False)

    class Meta:
        verbose_name = _("Состояние")
        verbose_name_plural = _("Состояния")
