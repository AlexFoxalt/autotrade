import os

from celery import Celery

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "autotrade_main.settings.base")
app = Celery("autotrade")
app.config_from_object("django.conf:settings", namespace="CELERY")
app.autodiscover_tasks()
