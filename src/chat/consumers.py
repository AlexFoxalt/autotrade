from datetime import datetime
import json
from channels.generic.websocket import AsyncWebsocketConsumer


class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_name = self.scope["url_route"]["kwargs"]["room_name"]
        self.room_group_name = "chat_%s" % self.room_name

        await self.channel_layer.group_add(self.room_group_name, self.channel_name)

        await self.accept()

    async def disconnect(self, close_code):
        await self.channel_layer.group_discard(self.room_group_name, self.channel_name)

    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json["message"]
        if message:
            await self.channel_layer.group_send(
                self.room_group_name, {"type": "chat_message", "message": message}
            )

    async def chat_message(self, event):
        message = event["message"]
        user = self.scope["user"]
        username = user.email if user.is_authenticated else "anonymous"
        time = datetime.now().strftime("%-H:%M:%S")

        await self.send(
            text_data=json.dumps(
                {"message": message, "user": username, "time": f"[{time}] "}
            )
        )
