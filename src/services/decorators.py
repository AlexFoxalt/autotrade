from rest_framework import exceptions


def admin_only(func):
    def wrapper(self, request, *args, **kwargs):
        if not request.user.is_superuser:
            raise exceptions.PermissionDenied(
                detail="No permission: admin only", code=403
            )
        return func(self, request, *args, **kwargs)

    return wrapper
