import logging

warning_logger = logging.getLogger("warning_logger")
debug_logger = logging.getLogger("debug_logger")


def colorize(color, string):
    colors = {
        "Black": "\u001b[30m",
        "Red": "\u001b[31m",
        "Green": "\u001b[32m",
        "Yellow": "\u001b[33m",
        "Blue": "\u001b[34m",
        "Magenta": "\u001b[35m",
        "Cyan": "\u001b[36m",
        "White": "\u001b[37m",
    }
    default = colors.get("White")
    reset = "\u001b[0m"
    return f"{colors.get(color, default)}{string}{reset}"
