from rest_framework_simplejwt.authentication import JWTAuthentication

from accounts.models import Client
from api.serializers import PublicationSerializer, ClientSerializer
from autos.models import Announcement
from autos.models_choices import Kind, Condition, Color
from base.models import Publication


class ContextMixin:
    def get_extra_context(self, **kwargs):
        context = kwargs
        context["categories"] = Kind.objects.all()

        url_name = self.request.resolver_match.url_name
        if url_name == "index":
            context["conditions"] = Condition.objects.all()
            context["colors"] = Color.objects.all()
            context["publications"] = Publication.get_random_active_publications(3)
            context["popular_announcements"] = Announcement.get_n_popular_announcements(
                2
            )
            context["latest_announcements"] = Announcement.get_n_latest_announcements(8)
        elif url_name == "contact":
            context["room_name"] = "autotrade_chat"

        return context


class PublicationAPIMixin:
    serializer_class = PublicationSerializer
    queryset = Publication.objects.all()
    lookup_field = "slug"


class ClientAPIMixin:
    serializer_class = ClientSerializer
    queryset = Client.objects.all()
    lookup_field = "uuid"
    authentication_classes = (JWTAuthentication,)
