from django.core.handlers.wsgi import WSGIRequest
from django.http import JsonResponse

from autos.models import Announcement
from base.models import Favorite


def add_to_favorite_or_return_error_alert(request: WSGIRequest) -> JsonResponse:
    post_id = int(request.POST.get("post_id"))
    announcement = Announcement.objects.get(pk=post_id)
    if announcement:
        if Favorite.objects.filter(client=request.user.id, announcement=announcement):
            return JsonResponse({"status": "Объявление уже находится в Избранном"})
        else:
            Favorite.objects.create(client=request.user, announcement=announcement)
            return JsonResponse({"status": "Успешно добавлено в избранное"})
    else:
        return JsonResponse({"status": "Не найдено такого объявления"})


def remove_from_favorite_or_return_error_alert(request: WSGIRequest) -> JsonResponse:
    post_id = int(request.POST.get("post_id"))
    announcement = Announcement.objects.get(pk=post_id)
    if announcement:
        favorite_object = Favorite.objects.filter(
            client=request.user.id, announcement=announcement
        )
        if favorite_object:
            favorite_object.delete()
            return JsonResponse({"status": "Объявление удалено из Избранного"})
        else:
            return JsonResponse({"status": "Объявление не найдено в Избранном"})
    else:
        return JsonResponse({"status": "Не найдено такого объявления"})
