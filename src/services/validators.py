import re

from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _


def telegram_link_validator(value: str):
    """Simple validation of telegram link. ex: https:///t.me/username"""
    if not re.match(r"https://t.me/\w+", value):
        raise ValidationError(
            _("Неверный формат ссылки на Телеграм профиль."),
            params={"value": value},
        )
