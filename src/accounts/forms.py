from django.contrib.auth.forms import AuthenticationForm, UserCreationForm

from accounts.models import Client


class LoginForm(AuthenticationForm):
    pass


class RegistrationForm(UserCreationForm):
    class Meta:
        model = Client
        fields = ["email", "first_name", "last_name", "password1", "password2"]
