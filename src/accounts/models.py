from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.core.mail import send_mail
from django.db import models
from django.utils.translation import gettext_lazy as _
from phonenumber_field.modelfields import PhoneNumberField

from accounts.managers import ClientManager
from autos.models_choices import City
from services.functions import client_directory_path, generate_uuid
from services.validators import telegram_link_validator


class Client(AbstractBaseUser, PermissionsMixin):
    """Default model for describing user on this project"""

    email = models.EmailField(
        _("Емейл"),
        unique=True,
        help_text=_("Обязательное поле! Введите емейл в формате: my_email@example.com"),
        error_messages={"unique": _("Пользователь с таким емейлом уже существует!")},
    )
    phone_number = PhoneNumberField(
        _("Номер телефона"), unique=True, blank=False, null=True
    )
    first_name = models.CharField(_("Имя"), max_length=150, blank=False)
    last_name = models.CharField(_("Фамилия"), max_length=150, blank=False)
    birthday = models.DateField(_("День Рождения"), null=True, blank=True)
    city = models.ForeignKey(
        verbose_name=_("Город"),
        null=True,
        to=City,
        blank=False,
        on_delete=models.CASCADE,
    )
    photo = models.ImageField(
        _("Фотография"),
        upload_to=client_directory_path,
        default="default_data/default_photo.png",
    )
    telegram = models.URLField(
        _("Телеграм"),
        validators=[telegram_link_validator],
        blank=True,
        null=True,
        unique=True,
        error_messages={
            "unique": _("Этот телеграм уже используется другим пользователем!")
        },
        help_text=_("Введите ссылку на свой телеграм в формате: https://t.me/username"),
    )
    uuid = models.UUIDField(default=generate_uuid, db_index=True, unique=True)

    date_joined = models.DateTimeField(_("Дата создания"), auto_now_add=True)
    is_staff = models.BooleanField(
        _("Персонал"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("Активирован"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. "
            "Unselect this instead of deleting accounts."
        ),
    )

    objects = ClientManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("Клиент")
        verbose_name_plural = _("Клиенты")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def email_user(self, subject, message, from_email=None, **kwargs):
        """Send an email to this user."""
        send_mail(subject, message, from_email, [self.email], **kwargs)

    def get_list_of_favorite_announcements(self):
        return [instance.announcement.pk for instance in self.favorite_client.all()]
