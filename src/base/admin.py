from django.contrib import admin

from base.models import Favorite, Publication


class PublicationAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}


admin.site.register(Favorite)
admin.site.register(Publication, PublicationAdmin)
