from celery import shared_task
from django.conf import settings

from django.core.mail import send_mail

from services.functions import parse_usd_rate


@shared_task
def send_usd_rate_email():
    subject = "USD rate from autotrade :)"
    message = parse_usd_rate()
    email_from = settings.EMAIL_HOST_USER
    recipient_list = ["alllexey1997@gmail.com"]
    send_mail(subject, message, email_from, recipient_list, fail_silently=False)
    return "Email sent successfully!"
