from django.contrib.auth import login
from django.contrib.auth.views import LoginView, LogoutView
from django.http import HttpResponseRedirect, JsonResponse
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import (
    TemplateView,
    DetailView,
    ListView,
    CreateView,
)

from accounts.forms import LoginForm, RegistrationForm
from accounts.models import Client
from autos.models import Announcement
from base.models import Publication
from services.db_functions import (
    add_to_favorite_or_return_error_alert,
    remove_from_favorite_or_return_error_alert,
)
from services.functions import combine_context
from services.mixins import ContextMixin


class Index(ContextMixin, TemplateView):
    template_name = "index.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context(
            name=self.request.resolver_match.url_name
        )
        return combine_context(context, extra_context)


class Contact(ContextMixin, TemplateView):
    template_name = "contact.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)


class Login(ContextMixin, LoginView):
    template_name = "auth/login.html"
    authentication_form = LoginForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)

    def get_success_url(self):
        return reverse_lazy("index")


class Logout(LogoutView):
    next_page = "index"


class Register(ContextMixin, CreateView):
    template_name = "auth/register.html"
    model = Client
    form_class = RegistrationForm

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)

    def get_success_url(self):
        return reverse_lazy("index")

    def form_valid(self, form):
        client = form.save()
        login(self.request, client)
        return HttpResponseRedirect(reverse("index"))


class ShowPublication(ContextMixin, DetailView):
    template_name = "publication.html"
    model = Publication

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)

    def get_object(self, queryset=None):
        return self.model.increase_views_counter_by_one_and_return_object(self.kwargs)


class AllAnnouncements(ContextMixin, ListView):
    template_name = "all_announcements.html"
    model = Announcement
    context_object_name = "announcements"
    paginate_by = 6

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)


class ShowAnnouncement(ContextMixin, DetailView):
    template_name = "announcement.html"
    model = Announcement

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        extra_context = self.get_extra_context()
        return combine_context(context, extra_context)

    def get_object(self, queryset=None):
        return self.model.increase_views_counter_by_one_and_return_object(self.kwargs)


class AddOrRemoveFavorite(View):
    """Adds/removes Favorite model instance via Ajax"""

    def post(self, request, *args, **kwargs):
        if request.POST.get("status") == "add":
            return add_to_favorite_or_return_error_alert(request)
        elif request.POST.get("status") == "remove":
            return remove_from_favorite_or_return_error_alert(request)
        else:
            return JsonResponse({"status": "error"})
