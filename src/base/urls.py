from django.conf.urls import url
from django.urls import path
from django.views.generic import RedirectView

from base.views import (
    Index,
    Contact,
    ShowPublication,
    AllAnnouncements,
    ShowAnnouncement,
    Login,
    Logout,
    Register,
    AddOrRemoveFavorite,
)

# fmt: off
urlpatterns = [
    path("", Index.as_view(), name="index"),
    path("contact/", Contact.as_view(), name="contact"),

    path("login/", Login.as_view(), name="login"),
    path("logout/", Logout.as_view(), name="logout"),
    path("register/", Register.as_view(), name="register"),

    path("publication/<slug:publication_slug>", ShowPublication.as_view(), name="publication"),
    path("announcement/<uuid:uuid>", ShowAnnouncement.as_view(), name="announcement"),
    path("announcements/", AllAnnouncements.as_view(), name="announcements"),
    path("create-remove-announcement/", AddOrRemoveFavorite.as_view(), name="create-remove-announcement"),

    url(r"^favicon\.ico$", RedirectView.as_view(url="/autotrade/static/image/favicon.ico")),
]
# fmt: on
