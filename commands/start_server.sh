#!/bin/bash

python manage.py migrate
python manage.py collectstatic --noinput
python manage.py loaddata db_data.json
daphne -b 0.0.0.0 -p "${ASGI_PORT}" autotrade_main.asgi:application