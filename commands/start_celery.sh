#!/bin/bash

celery -A autotrade_main worker -l info -c "${CELERY_NUM_WORKERS}"