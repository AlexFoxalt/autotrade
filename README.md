# autotrade

- Description:   AlexFoxalt pet project
- Technology:   Python-Django
- Essence:         Web site with which you can trade cars

## Installation
1. Clone from repository              `git clone <ssh>`
2. Go to project folder               `cd <repo>`
3. Install venv, if it not installed  `pip install virtualenv`
4. Create new venv                    `virtualenv <name>`
5. Activate venv                      `source venv/bin/activate`
6. Install all required moduls        `pip install -r requirements.txt`
7. Create DB                          `python manage.py migrate`
8. Fill the DB by required data       `python manage.py fill_db`
9. (optional) Add some test instances `python manage.py create_example 10`
or you can load example db            `python manage.py loaddata db_data.json`
10. Done! You can run server now!

## Name
Autrotrade.ua

## Description
Website for selling/buying cars by users
