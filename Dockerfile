FROM python:3.10

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

RUN apt update

RUN mkdir /autotrade
RUN mkdir /autotrade/commands

WORKDIR /autotrade

COPY ./src ./
COPY ./commands ./commands
COPY ./requirements.txt ./requirements.txt
COPY ./nginx ./nginx

RUN python -m pip install --upgrade pip
RUN pip install -r requirements.txt

CMD ["bash"]